import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type itemlist from "./type/itemlist";

export const useItemStore = defineStore("Item", () => {
  const itesmList = ref<itemlist[]>([]);
  const sumList = ref(0);
  const Item = ref<itemlist>({
    id: -1,
    name: "",
    price: 0,
  });

  const clear = () => {
    Item.value = {
      id: -1,
      name: "",
      price: 0,
    };
  };

  const SumList = () => {
    sumList.value = 0;
    for (let i = 0; i < itesmList.value.length; i++) {
      sumList.value += itesmList.value[i].price;
    }
  };

  const Additem = (menus: itemlist) => {
    Item.value.id = menus.id;
    Item.value.name = menus.name;
    Item.value.price = menus.price;
    itesmList.value.push(Item.value);
    clear();
    SumList();
  };

  return { itesmList, Additem, sumList };
});

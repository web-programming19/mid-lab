export default interface itemlist {
  id: number;
  name: string;
  price: number;
}
